//
//  AFWrapper.swift
//  PlayersPathway
//
//  Created by Impero-Azharhussain on 29/05/17.
//  Copyright © 2016 Impero It. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject  {
     
     
     class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        print("\nEndPoint:: "+strURL)
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
               if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    success(resJson)
                    print("\nResponse::")
                    print(resJson)
               }
               if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
               }
          }
     }
     
     class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
          
        print("\nEndPoint:: "+strURL)
        print("\nRequestParams::")
        print(params)
          Alamofire.request(strURL, method: .post, parameters: params, headers: headers).responseJSON { (responseObject) -> Void in
                              
               if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    success(resJson)
                print("\nResponse::")
                print(resJson)
               }
               if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
               }
          }
     }
     class func requestGETAuthURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        print("\nEndPoint:: "+strURL)
        print("\nRequestParams::")
        print(params)
          Alamofire.request(strURL, method: .get, parameters: params, headers: headers).responseJSON { (responseObject) -> Void in
               
               print(responseObject)
               
               if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    success(resJson)
                print("\nResponse::")
                print(resJson)
               }
               if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
               }
          }
     }
     
     
     
     class func requestPUTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        print("\nEndPoint:: "+strURL)
        print("\nRequestParams::")
        print(params)
          Alamofire.request(strURL, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
               
               print(responseObject)
               
               if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    success(resJson)
                print("\nResponse::")
                print(resJson)
               }
               if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
               }
          }
     }
     
     
     
}
