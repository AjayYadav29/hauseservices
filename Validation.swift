

import UIKit
import Foundation
class Validation: NSObject {
   static let validation = Validation()
    
    
    func isValidEmail(_ Emailid: String) -> Bool {
        let regExPattern: String = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"
        let regEx = try? NSRegularExpression(pattern: regExPattern, options: .caseInsensitive)
        let regExMatches: Int = (regEx?.numberOfMatches(in: Emailid, options: [], range: NSRange(location: 0, length: (Emailid.count ))))!
        if regExMatches == 0 {
            return false
        }
        else {
            //        NSString *noDots=[Emailid stringByReplacingOccurrencesOfString:@"." withString:@""];
            //        NSString *modified=[noDots stringByReplacingOccurrencesOfString:@"@" withString:@""];
            //        if (![self isValidLanguage:modified]) {
            //            return NO;
            //        }
            return true
        }
    }
    func isEmpty(_ Field: Any) -> Bool {
        var value: String = (Field as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (value.count ) == 0 {
            return true
        }
        return false
    }
    func checkMinLength(_ Field: String, withLimit len: Int) -> Bool {
        if (Field.count ) >= len {
            return true
        }
        return false
    }
    func checkMaxLength(_ Field: String, withLimit len: Int) -> Bool {
        if (Field.count ) <= len {
            return true
        }
        return false
    }
    
    func checkEqual(_ first: String, withField second: String) -> Bool {
        if (first == second) {
            return true
        }
        return false
    }
    func isValidText(_ name: String) -> Bool {
        let regExPattern: String = "^[A-Za-z]+$"
        let regEx = try? NSRegularExpression(pattern: regExPattern, options: .caseInsensitive)
        let regExMatches: Int = (regEx?.numberOfMatches(in: name, options: [], range: NSRange(location: 0, length: (name.count ))))!
        if regExMatches == 0 {
            return false
        }
        else {
            return true
        }
    }
    
    func isValidPassword(_ pwd: String) -> Bool {
        if (pwd.count ) < 6 || (pwd.count ) > 32 {
            return false
        }
        // too long or too short
        var rang: NSRange
        rang = (pwd as NSString).rangeOfCharacter(from: CharacterSet.letters)
        if rang.length == 0 {
            return false
        }
        // no letter
        rang = (pwd as NSString).rangeOfCharacter(from: CharacterSet.decimalDigits)
        if rang.length == 0 {
            return false
        }
        // no number;
        return true
    }
//    func isValidLanguage(_ str: String) -> Bool {
//        var isHaveSpecialChar: Bool = true
//        if !self.isFString(str) {
//            isHaveSpecialChar = false
//        }
//        else {
//            isHaveSpecialChar = true
//        }
//        return isHaveSpecialChar
//    }
//    func isFString(_ string: String) -> Bool {
//        var isValid: Bool = true
//        for i in 0..<(string.characters.count) {
//            if !(Int(string[i]) >= 65 && Int(string[i]) <= 90) && !(Int(string[i]) >= 97 && Int(string[i]) <= 122) && !(Int(string[i]) >= 192 && Int(string[i]) <= 214) && !(Int(string[i]) >= 216 && Int(string[i]) <= 246) && !(Int(string[i]) >= 247 && Int(string[i]) <= 255) && !(Int(string[i]) == 257) && !(Int(string[i]) == 275) && !(Int(string[i]) == 299) && !(Int(string[i]) == 333) && !(Int(string[i]) == 339) && !(Int(string[i]) == 380) && !(Int(string[i]) == 381) && !(Int(string[i]) == 378) && !(Int(string[i]) == 363) && !(Int(string[i]) == 353) && !(Int(string[i]) == 223) && !(Int(string[i]) == 347) && !(Int(string[i]) == 322) && !(Int(string[i]) == 269) && !(Int(string[i]) == 231) && !(Int(string[i]) == 263) && !(Int(string[i]) == 241) && !(Int(string[i]) == 324) && !(Int(string[i]) == 200) && !(Int(string[i]) == 382) && !(Int(string[i]) == 201) && !(Int(string[i]) == 202) && !(Int(string[i]) == 203) {
//                isValid = false;
//                break;
//            }
           // }
           // }
    
    
    
}
extension UICollectionView
{
    func SetTableViewBlankLable(count:Int,str:String)
    {
        if(count==0)
        {
            removeOldLable()
            let lblText:UILabel = UILabel()
            // lblText.frame = CGRectMake(0, 0, self.frame.width, self.frame.height)
            lblText.frame = self.frame
            lblText.numberOfLines = 0
            lblText.text = str
            lblText.textAlignment = .center
            lblText.textColor = UIColor.lightGray
            self.backgroundColor = UIColor.clear
            lblText.tag = 987654
            self.superview!.insertSubview(lblText, belowSubview: self)
        }
        else
        {
            removeOldLable()
        }
    }
    func removeOldLable()
    {
        for view in self.superview!.subviews
        {
            if(view is UILabel)
            {
                if(view.tag == 987654)
                {
                    view.removeFromSuperview()
                }
            }
        }
    }
}
extension UITableView
{
    func SetTableViewBlankLable(count:Int,str:String)
    {
        if(count==0)
        {
            removeOldLable()
            let lblText:UILabel = UILabel()
            // lblText.frame = CGRectMake(0, 0, self.frame.width, self.frame.height)
            lblText.frame = self.frame
            lblText.numberOfLines = 0
            lblText.text = str
            lblText.textAlignment = .center
            lblText.textColor = UIColor.lightGray
            self.backgroundColor = UIColor.clear
            self.separatorStyle = .none
            lblText.tag = 987654
            self.superview!.insertSubview(lblText, belowSubview: self)
        }
        else
        {
            removeOldLable()
            self.separatorStyle = .singleLine
        }
    }
    func removeOldLable()
    {
        for view in self.superview!.subviews
        {
            if(view is UILabel)
            {
                if(view.tag == 987654)
                {
                    view.removeFromSuperview()
                }
            }
        }
    }
}

