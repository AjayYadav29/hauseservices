//
//  getImages.swift
//  PropertyTurnover
//
//  Created by Impero IT on 27/09/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class getImages: NSObject,Mappable {

    var Status = 0
    var Message = ""
    var Result : [result4] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}
class result4 : NSObject ,Mappable{
    
    var ImageUrl = ""
    override init() {
        
    }
    required init?(map: Map) {
        ImageUrl <- map["ImageUrl"]
    }
    func mapping(map: Map) {
        ImageUrl <- map["ImageUrl"]
    }
}
