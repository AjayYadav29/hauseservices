//
//  getSeasonalModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 05/10/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class getSeasonalModel :NSObject,Mappable {
    
    var Status = 0
    var Message = ""
    var Result : [result5] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}

class result5 : NSObject ,Mappable{
    
    var category_name = ""
    var img_url = ""
    override init() {
        
    }
    required init?(map: Map) {
        category_name <- map["category_name"]
        img_url <- map["img_url"]
    }
    func mapping(map: Map) {
        category_name <- map["category_name"]
        img_url <- map["img_url"]
    }
}
