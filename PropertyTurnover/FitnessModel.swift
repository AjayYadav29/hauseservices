//
//  FitnessModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 27/09/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class FitnessModel: NSObject , Mappable {

    var Status = 0
    var Message = ""
    var Result : [result2] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}
class result2 : NSObject ,Mappable{
    var ServiceID = ""
    var ServiceName = ""
    var RatePerHours = ""
    var ServiceCode = ""
    var CategoryId = 0
    var SubcategoryID = 0
    var ImageName = ""
    override init() {
        
    }
    required init?(map: Map) {
        ServiceID <- map["ServiceID"]
        ServiceName <- map["ServiceName"]
        RatePerHours <- map["RatePerHours"]
        ServiceCode <- map["ServiceCode"]
        CategoryId <- map["CategoryId"]
        SubcategoryID <- map["SubcategoryID"]
        ImageName <- map["ImageUrl"]
    }
    func mapping(map: Map) {
        ServiceID <- map["ServiceID"]
        ServiceName <- map["ServiceName"]
        RatePerHours <- map["RatePerHours"]
        ServiceCode <- map["ServiceCode"]
        CategoryId <- map["CategoryId"]
        SubcategoryID <- map["SubcategoryID"]
        ImageName <- map["ImageUrl"]
    }
}
