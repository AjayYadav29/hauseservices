//
//  Categories.swift
//  PropertyTurnover
//
//  Created by admin on 17/06/20.
//  Copyright © 2020 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
import NVActivityIndicatorView

class Categories: NSObject ,Mappable,NVActivityIndicatorViewable{
    
    var RatePerHours : String!
    var sub_category : String!
    var main_category : String!
    var id : String!
    var ServiceName : String!
    var subcategories : Subcategories!
    

    
    required init?(map: Map) {
          RatePerHours <- map["RatePerHours"]
          sub_category <- map["sub_category"] //SubcategoryName
          main_category <- map["main_category"]
          id <- map["id"]
          ServiceName <- map["ServiceName"]

      }
    func mapping(map: Map) {
        RatePerHours <- map["RatePerHours"]
        sub_category <- map["sub_category"] //SubcategoryName
        main_category <- map["main_category"]
        id <- map["id"]
        ServiceName <- map["ServiceName"]
      //  getData()
    }
    

}

