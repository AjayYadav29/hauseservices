//
//  AppDelegate.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import CoreLocation
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var isMap = true
    var isLocationCheck = false
    var buildVersion = ""
    var shareManager : Globals = Globals.sharedInstance
    var locationManager : CLLocationManager?
    var coordinate : CLLocationCoordinate2D!

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GetCurrentLocation()
        
        IQKeyboardManager.shared.enable = true
       // Fabric.with([Crashlytics.self])

        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"]  as? String {
            buildVersion = text
            if UserDefaults.standard.string(forKey: "version") != nil{
                let str = UserDefaults.standard.string(forKey: "version")
                if Float(str!)! != Float(buildVersion)!{
                    UserDefaults.standard.set(text, forKey: "version")
                    moveToIntroView()
                }
                else{
                    if UserDefaults.standard.bool(forKey: Constants.KEYS.INTROVIEW) == true{
                        moveToDashBoard()
                    }else{
                        moveToIntroView()
                    }
                }
            }else{
                UserDefaults.standard.set(text, forKey: "version")
                moveToIntroView()
            }
        }
        
        
        return true
    }
    func moveToDashBoard()
    {
        

        if UserDefaults.standard.value(forKey: "isAlreadyLogin") as! Bool? == true {
            let login : DashBoardVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            let nav: UINavigationController = UINavigationController(rootViewController: login)
            nav.isNavigationBarHidden=true
            self.window?.rootViewController = nav

        }else {
            let loginVC : LoginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav: UINavigationController = UINavigationController(rootViewController: loginVC)
            nav.isNavigationBarHidden=true
            self.window?.rootViewController = nav

        }
    }
    func moveToIntroView()
    {
        let login : IntroViewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IntroViewVC") as! IntroViewVC
        let nav: UINavigationController = UINavigationController(rootViewController: login)
        nav.isNavigationBarHidden=true
        self.window?.rootViewController = nav
    }
    func GetCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.startUpdatingLocation()
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.pausesLocationUpdatesAutomatically = false
    }
    
    func askEnableLocationService() ->String {
        var showAlertSetting = false
        var showInitLocation = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                showAlertSetting = true
                print("HH: kCLAuthorizationStatusDenied")
            case .restricted:
                showAlertSetting = true
                print("HH: kCLAuthorizationStatusRestricted")
            case .authorizedAlways:
                showInitLocation = true
                print("HH: kCLAuthorizationStatusAuthorizedAlways")
            case .authorizedWhenInUse:
                showInitLocation = true
                print("HH: kCLAuthorizationStatusAuthorizedWhenInUse")
            case .notDetermined:
                showInitLocation = true
                print("HH: kCLAuthorizationStatusNotDetermined")
            default:
                break
            }
        }else{
            showAlertSetting = true
            print("HH: locationServicesDisabled")

        }
        if showAlertSetting {
            let alertController = UIAlertController(title: "xxxxxx", message: "Please enable location service in the settings", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in



                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }

            }
            alertController.addAction(OKAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion:nil)

        }
        if showInitLocation {

            return "YES"

        }
        return "NO"

    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
     //   self.shareManager.iSLocation = true
            coordinate = manager.location!.coordinate
            let locValue : CLLocationCoordinate2D = manager.location!.coordinate
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            let location = manager.location?.coordinate
        
        if isMap{
            getAddressFromLatLon(pdblLatitude: "\(locValue.latitude)", withLongitude: "\(locValue.longitude)")
            isMap = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        isLocationCheck = true
        if status == .authorizedWhenInUse {
            
//            locationManager.requestAlwaysAuthorization()
//            let alert = UIAlertController(title: "You must provide always in use Location", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertControllerStyle.alert)
//            
//            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//                
//                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString) as! URL)
//                
//            }))
//            
//            self.shareManager.iSLocation = false
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
        if status == .authorizedAlways
        {
            locationManager?.startUpdatingLocation()
            locationManager?.startMonitoringSignificantLocationChanges()
        }
        if status == .denied
        {
//            locationManager.startUpdatingLocation()
//            locationManager.startMonitoringSignificantLocationChanges()
//            let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//               // UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString) as! URL)
//
//            }))
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//            self.shareManager.iSLocation = false
            
//                locationManager?.startUpdatingLocation()
//                locationManager?.startMonitoringSignificantLocationChanges()
//                let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//                    UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString) as! URL)
//
//                }))
//                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//                self.shareManager.iSLocation = false
                let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                              alert.dismiss(animated: true, completion: nil)
                          }))
                         
                          self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                          self.shareManager.iSLocation = false
            
        }
        if status == .notDetermined
        {
//            locationManager.startUpdatingLocation()
//            locationManager.startMonitoringSignificantLocationChanges()
//            let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//                alert.dismiss(animated: true, completion: nil)
//            }))
//           
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//            self.shareManager.iSLocation = false
        }
        if status == .restricted
        {
//            locationManager.startUpdatingLocation()
//            locationManager.startMonitoringSignificantLocationChanges()
//            let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString) as! URL)
//            }))
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//            self.shareManager.iSLocation = false
            
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks != nil{
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        print(pm.administrativeArea)
                        
                        
                        // self.shareManager.stateglobal =
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            self.shareManager.cityglobal = pm.locality!
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                            self.shareManager.zipcodeglobal = pm.postalCode!
                        }
                        if pm.administrativeArea != nil{
                            self.shareManager.stateglobal = pm.administrativeArea!
                        }
                        print(addressString)
                    }
                }
                
        })
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reload"), object: nil)

        
//        if isLocationCheck{
//            if CLLocationManager.locationServicesEnabled() {
//                switch(CLLocationManager.authorizationStatus()) {
//                case .notDetermined, .restricted, .denied:
//                    let alert = UIAlertController(title: "Location is Disabled", message:"Go to your Settings App > Privacy > Location Services", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
//                        UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString) as! URL)
//
//                    }))
//                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//                    self.shareManager.iSLocation = false
//                case .authorizedAlways, .authorizedWhenInUse:
//                    print("Access")
//                }
//
//            } else {
//                print("Location services are not enabled")
//            }
//        }
//        else{
//
//        }
        
        
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()


    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

