//
//  StateModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 27/09/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class StateModel: NSObject,Mappable {

    var Status = 0
    var Message = ""
    var Result : [resultstate] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}
class resultstate : NSObject,Mappable{
    
    var StateID = 0
    var StateName = ""
    
    override init() {
        
    }
    required init?(map: Map) {
        StateID <- map["id"]
        StateName <- map["state"]
    }
    func mapping(map: Map) {
        StateID <- map["id"]
        StateName <- map["state"]
    }
}
