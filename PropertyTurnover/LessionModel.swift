//
//  LessionModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 27/09/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class LessionModel: NSObject,Mappable {
 
    var Status = 0
    var Message = ""
    var Result : [Categories] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}
class result1 : NSObject ,Mappable{
    
    var SubcategoryID = ""
    var SubcategoryName = ""
    var ImageName = ""
    var CategoryID = ""
    
    override init() {
        
    }
    required init?(map: Map) {
        SubcategoryID <- map["SubcategoryID"]
        SubcategoryName <- map["category"] //SubcategoryName
        ImageName <- map["ImageUrl"]
        CategoryID <- map["CategoryID"]
    }
    func mapping(map: Map) {
        SubcategoryID <- map["SubcategoryID"] //SubcategoryName
        SubcategoryName <- map["category"]
        ImageName <- map["ImageUrl"]
        CategoryID <- map["CategoryID"]
    }
}
