//
//  getDashBoardDataModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 26/09/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class getDashBoardDataModel: NSObject ,Mappable {

    var Status = 0
    var Message = ""
    var Result : [result] = []
    
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
        
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}
class result : NSObject,Mappable{
    
    var CategoryId = ""
    var CategoryName = ""
    var ImageName = ""
    var sub_category : [SubCategory] = []
    
    override init() {
        
    }
    required init?(map: Map) {
        CategoryId <- map["cat_id"]
        CategoryName <- map["category_name"]
        ImageName <- map["img_url"]
        sub_category <- map["sub_category"]

    }
    func mapping(map: Map) {
        CategoryId <- map["cat_id"]
        CategoryName <- map["category_name"]
        ImageName <- map["img_url"]
        sub_category <- map["sub_category"]
    }
}
