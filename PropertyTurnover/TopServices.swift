//
//  TopServices.swift
//  PropertyTurnover
//
//  Created by admin on 27/06/20.
//  Copyright © 2020 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper

class TopServices: NSObject,Mappable {
    
    var cat_id : String = ""
    var category_name : String = ""
    var img_url : String = ""
    
    override init() {
    }
    
    required init?(map: Map) {
        cat_id <- map["cat_id"]
        category_name <- map["category_name"]
        img_url <- map["img_url"]
    }
    func mapping(map: Map) {
        cat_id <- map["cat_id"]
        category_name <- map["category_name"]
        img_url <- map["img_url"]
    }
}
