//
//  Subcategories.swift
//  PropertyTurnover
//
//  Created by admin on 18/06/20.
//  Copyright © 2020 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper

class Subcategories: NSObject ,Mappable {
 
    var Status = 0
    var Message = ""
    var innerCategory : [Categories] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        innerCategory <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        innerCategory <- map["map"]
    }
}
