//
//  SubCategory.swift
//  PropertyTurnover
//
//  Created by Ajay Yadav on 04/05/19.
//  Copyright © 2019 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper

class SubCategory : NSObject ,Mappable{
    
    var id = ""
    var sub_category = ""
    var sub_category_id = ""
    var image_url = ""
    var cat_id = ""
    var inner_category : [Innercategory] = []


   override init() {
        
    }
    required init?(map: Map) {
        id <- map["id"]
        sub_category <- map["sub_category"]
        sub_category_id <- map["sub_category_id"]
        image_url <- map["image_url"]
        cat_id <- map["cat_id"]
        inner_category <- map["inner_category"]
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        sub_category <- map["sub_category"]
        sub_category_id <- map["sub_category_id"]
        image_url <- map["image_url"]
        cat_id <- map["cat_id"]
        inner_category <- map["inner_category"]

    }
}

