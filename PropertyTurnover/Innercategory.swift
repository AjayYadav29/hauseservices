//
//  Innercategory.swift
//  PropertyTurnover
//
//  Created by Ajay Yadav on 04/05/19.
//  Copyright © 2019 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper

class Innercategory: NSObject,Mappable {
    
    var id = ""
    var inner_category = ""
    var sub_category_id = ""
    var image_url = ""
    var cat_id = ""
    var RatePerHours = ""
    var ServiceID = ""
    var ServiceCode = ""
    var main_category = ""
    
    
    
    
    override init() {
        
    }
    required init?(map: Map) {
        id <- map["id"]
        inner_category <- map["inner_category"]
        sub_category_id <- map["sub_category_id"]
        image_url <- map["image_url"]
        cat_id <- map["cat_id"]
        RatePerHours <- map["RatePerHours"]
        ServiceID <- map["ServiceID"]
        ServiceCode <- map["ServiceCode"]
        main_category <- map["main_category"]

        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        inner_category <- map["inner_category"]
        sub_category_id <- map["sub_category_id"]
        image_url <- map["image_url"]
        cat_id <- map["cat_id"]
        RatePerHours <- map["RatePerHours"]
        ServiceID <- map["ServiceID"]
        ServiceCode <- map["ServiceCode"]
        main_category <- map["main_category"]

    }
}
