//
//  getServiceListModel.swift
//  PropertyTurnover
//
//  Created by Impero IT on 05/10/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper
class getServiceListModel: NSObject,Mappable {

    var Status = 0
    var Message = ""
    var Result : [result7] = []
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["Result"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["Result"]
    }
}
class result7 : NSObject ,Mappable{
    var ServiceID = ""
    var ServiceName = ""
    var RatePerHours = ""
    var ServiceCode = ""
    var CategoryId = ""
    var SubcategoryID = ""
    var ImageName = ""
    override init() {
        
    }
    required init?(map: Map) {
        ServiceID <- map["ServiceID"]
        ServiceName <- map["ServiceName"]
        RatePerHours <- map["RatePerHours"]
        ServiceCode <- map["ServiceCode"]
        CategoryId <- map["CategoryId"]
        SubcategoryID <- map["SubcategoryID"]
        ImageName <- map["ImageUrl"]
    }
    func mapping(map: Map) {
        ServiceID <- map["ServiceID"]
        ServiceName <- map["ServiceName"]
        RatePerHours <- map["RatePerHours"]
        ServiceCode <- map["ServiceCode"]
        CategoryId <- map["CategoryId"]
        SubcategoryID <- map["SubcategoryID"]
        ImageName <- map["ImageUrl"]
    }
}
