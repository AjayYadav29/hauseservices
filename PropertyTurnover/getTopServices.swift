//
//  getTopServices.swift
//  PropertyTurnover
//
//  Created by Ekansh Srivastava on 16/02/19.
//  Copyright © 2019 Impero IT. All rights reserved.
//

import UIKit
import ObjectMapper

class getTopServices: NSObject,Mappable {
    
    var Status = 0
    var Message = ""
    var Result : [TopServices] = []
    
    
    override init() {
        
    }
    required init?(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
    func mapping(map: Map) {
        Status <- map["status"]
        Message <- map["message"]
        Result <- map["map"]
    }
}

