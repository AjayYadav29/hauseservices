//
//  Constants.swift
//  BlueCoupon
//
//  Created by Impero IT on 03/06/16.
//  Copyright © 2016 Impero IT. All rights reserved.
//

import UIKit

struct Constants {

static let buildName = "Hospitality Vault"

struct Default {
     
    static let ACCESS_TOKEN = UserDefaults.standard.value(forKey: "BearerToken") as! String?
    static let USERNAME = UserDefaults.standard.value(forKey: "UserName") as! String?
    static let USERTYPE = UserDefaults.standard.value(forKey: "UserType") as! String?
    static let isUserLoggedIn = UserDefaults.standard.value(forKey: "isAlreadyLogin") as! Bool?

     
}
struct ScreenSize
{
     static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
     static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
     static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
     static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
     static let Is_IPhone_4_OR_Less =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
     static let Is_IPhone_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
     static let Is_IPhone_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
     static let Is_IPhone_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
     static let Is_IPad = UIDevice.current.userInterfaceIdiom == .pad
}

struct URLS {
    
    
    
    
     static let BASE_URL = "https://www.hauseservices.com/api/public/api/"//"https://propertyturnover.imperoserver.in/"
   //  static let NEW_BASE_URL = "https://www.hauseservices.com/api/public/api/"
    static let NEW_BASE_URL = "https://www.hauseservices.com/api/public/api/"
    
     static let hospitalityvault_master = BASE_URL + "hospitalityvault_master"
     static let Login = BASE_URL + "login"
     static let SignUpStaff = BASE_URL + "signup_staff"
     static let SetStaffProfile = BASE_URL + "SetStaffProfile"
     static let AddPhotoStaff = BASE_URL + "AddPhoto"
     static let SearchWorker = BASE_URL + "SearchWorkerDharmesh?"
     static let AddOrRemoveFavorites = BASE_URL + "AddOrRemoveFavorites"
     static let GetFavoriteslist = BASE_URL + "GetFavoriteslist?UserID="
     static let EditStaffProfile = BASE_URL + "EditStaffProfile"
     static let ChangePasswordStaff = BASE_URL + "ChangePassword"
     static let HelpStaff = BASE_URL + "Help"
     static let GetDocumentStaff =  BASE_URL + "getUserdocuments?UserID="
     static let UploadDocumentStaff = BASE_URL + "UploadDocuments"
     static let DeleteUserDocumentsStaff = BASE_URL + "DeleteUserDocuments?UserID="
     static let NotificationStaff = BASE_URL + "getNotification?UserID="
     static let SendRequestStaff = BASE_URL + "SendRequestStaff"
     static let SendRequestToFavorites = BASE_URL + "SendRequestToFavorites"
     static let StaffCompleteJob = BASE_URL + "StaffCompleteJob?StaffID="
     static let StaffConfirmJob = BASE_URL + "getConfirmJob?UserID="
     static let StaffPendingJob = BASE_URL + "getPendingJob?UserID="
     static let StaffJobAccept = BASE_URL + "StaffJobAccept"
     static let WorkerJobReject = BASE_URL + "WorkerJobReject?WorkerID="
     static let AddRating = BASE_URL + "AddRating"
     static let MESSAGESLISTSTAFF = BASE_URL + "messagesList?UserID="
     static let MESSAGEDETAILSTAFF = BASE_URL + "messages?myId="
     static let MESSAGESENDSTAFF = BASE_URL + "messages"
     static let getData = BASE_URL + "GetUserData"
    
    
     static let SubAccessLevels = BASE_URL + "SubAccessLevels/"
     static let GuestLogin = BASE_URL + "GuestLogin"
     static let MasterChallenges = BASE_URL + "MasterChallenges"
     static let ChallengesByMaster = BASE_URL + "ChallengesByMaster/"
     static let LinkResources = BASE_URL + "LinkResources"
     static let ResourceDetailById = BASE_URL + "ResourceDetailById/"
     static let RequestAccess = BASE_URL + "RequestAccess"
     static let AgeGroups = BASE_URL + "AgeGroups"
     static let SearchPlayers = BASE_URL + "SearchPlayers"
     static let AddPlayer = BASE_URL + "AddPlayer"
     static let TaskDetail = BASE_URL + "TaskDetail/"
     static let RecordPlayerScore = BASE_URL + "RecordPlayerScore"
     static let Demo = BASE_URL + "GoOffline/"
     static let RecordAllPlayerScore = BASE_URL + "RecordAllPlayerScore"
     static let EditProfile = BASE_URL + "EditProfile"
    
     static let viewCategory = NEW_BASE_URL + "houseservice"
     static let Subcategory = NEW_BASE_URL + "Subcategory"
     static let AddInquiry = NEW_BASE_URL + "AddInquiry"
    static let Service = NEW_BASE_URL + "Service"
    static let Images = NEW_BASE_URL + "Images"
     static let getState = NEW_BASE_URL + "GetState"
     static let Seasonal  = NEW_BASE_URL + "homeimprovement"
   // www.hauseservices.com/api/public/api/homeimprovement
    static let AllServices = NEW_BASE_URL + "Search?keyword="
    static let TopServices = NEW_BASE_URL + "popular"
   // www.hauseservices.com/api/public/api/houseservice
    static let innersubCategoryList = NEW_BASE_URL + "InnersubCategoryList"
    static let loginApi = NEW_BASE_URL + "userLogin"
    static let signupApi = NEW_BASE_URL + "userRegistration"
}

struct Errors {
    
    
    //Staff
     static let ERROR_EMPTY_EMAILID = "Please enter EmailId"
     static let ERROR_TIMEOUT = "Server error. Please try again later"
     static let ERROR_EMPTY_PASSWORD = "Please enter Password"
     static let ERROR_FULLNAME = "Please Enter FullName"
     static let ERROR_MOBILE = "Please Enter Mobile"
     static let ERROR_BUSINESSNAME = "Please Enter Business Name"
     static let ERROR_BUSINESSADDRESS = "Please Enter Business Address"
     static let ERROR_CITYSTAFF = "Please Select Correct City"
     static let ERROR_POSTCODE = "Please Enter PostCode"
     static let ERROR_BUSINESSTEXT = "Please Enter About Your Business"
     static let ERROR_SPECIALINFO = "Please Enter Special Info"
     static let ERROR_TERMSCONDITIONS = "Please Read Terms & Conditions"
     static let ERROR_CURRENTPASSWORDS = "Please Enter Current Password"
     static let ERROR_NEW_PASSWORDS = "Please Enter New Password"
     static let ERROR_PASSWORD_CONFIRMS = "Please Enter Confirm Password"
     static let ERROR_PASSWORD_MISMATCHS = "Confirm Password Not Match"
     static let ERROR_SUBJECTSTAFF = "Please Enter Subject"
     static let ERROR_QUESTIONSTAFF = "Please Enter Your Question"
     static let ERROR_STARTDATE = "Please Select Start Date"
     static let ERROR_ENDDATE = "Please Select End Date"
     static let ERROR_STARTTIME = "Please Select Start Time"
     static let ERROR_ENDTIME = "Please Select End Time"
     static let ERROR_VENUENAME = "Please Enter Venue Name"
     static let ERROR_VENUEADDRESS = "Please Enter Venue Address"
     static let ERROR_VENUEJOBDEATIL = "Please Enter Venue Job Details"
   //
    
     static let ERROR_EMAIL_ID = "Please enter valid EmailId"
     static let ERROR_FACEBOOK = "There was some error with Facebook Login"
     static let ERROR_FIRST_NAME = "Please enter valid First Name"
     static let ERROR_LAST_NAME = "Please enter valid Last Name"
     static let ERROR_DATE_OF_BIRTH = "Please select Date of Birth"
     static let ERROR_COUNTRY = "Please select Country"
     static let ERROR_STATE = "Please select State"
     static let ERROR_CITY = "Please select City"
     static let ERROR_GENDER = "Please select Gender"
     static let ERROR_PASSWORD = "Please enter valid Password"
     static let ERROR_OLD_PASSWORD = "Please enter valid old Password"
     static let ERROR_PASSWORD_MIN = "Password should be of minimum 8 characters"
     static let ERROR_PASSWORD_ALPHA_NUM = "Password should be Alpha numeric"
     static let ERROR_LOGIN = "There was some problem while login, try again later."
     static let ERROR_REGISTER = "There was some problem while registration, try again later."
     static let ERROR_INTERNTE = "No Internet available"
     static let ERROR_COMING_SOON = "Under Development"
     static let ERROR_PROFILE = "Please complete your profile before you can participate in Lucky Draw"
     static let ERROR_FRIEND = "You are already connected"
     static let ERROR_NOT_FRIEND = "You are not friend with this user. First become friend in order to chat"
     static let ERROR_PROFILE_FRIEND = "Please complete your profile before you can send Friend request"
     static let ERROR_CHAT = "You can not chat with yourself."
     static let ERROR_GROUP_FRIEND = "Please complete your profile before you can join group"
     static let ERROR_CHAT_FRIEND = "Please complete your profile before you can start chatting"
     static let ERROR_FRIEND_SELF = "You can not send friend request to yourself."
     static let ERROR_PUBLIC_PROFILE = "User profile is not public!"
     
     static let ERROR_OPINION = "Please enter valid opinion"
     static let ERROR_STATUS = "Please enter valid status"
     static let ERROR_MIN_STATUS = "Status should be of max 120 characters"
     
     static let ERROR_SUBJECT = "Please enter valid Subject"
     static let ERROR_MESSAGE = "Please enter valid Message"
    
    
}

struct  KEYS {
     static let LOGINKEY = "isLogin"
     static let USERINFO = "userInfo"
     static let ISCLIENT = "isClient"
     static let ISONTABLE = "isOnTable"
     static let BUSINESSNAME = "businessName"
     static let BUSINESSID = "businessId"
     static let TABLEUSERID = "tableUserId"
     static let USERSCREEN = "userscreen"
     static let MODEL = "model"
    static let INTROVIEW = "intro"
    static let fullname = "fullname"
    static let lastname = "lastname"
    static let email = "email"
    static let state = "state"
    static let city = "city"
    static let zipcode = "zipcode"
    static let message = "message"
    static let phoneNumber = "phonenumber"

    
     //Staff
     static let hospitalityMaster = "hospitalityvaultmaster"
}
struct NoData {
     static let FRIEND_BLOCK_TITLE = "Blocked Users"
     static let FRIEND_BLOCK_DESCRIPTION = "You don't have any blocked users list. All your blocked list will show up here."
     static let FRIEND_BLOCK_IMAGE = "ic_friend"
     
     static let FRIEND_REQUEST_TITLE = "Friend Request"
     static let FRIEND_REQUEST_DESCRIPTION = "You don't have any friend request yet. All your friend request will show up here."
     static let FRIEND_REQUEST_IMAGE = "ic_friend"
     
     static let FRIEND_TITLE = "Friends"
     static let FRIEND_DESCRIPTION = "You don't have any friends yet. All your friend  will show up here."
     static let FRIEND_IMAGE = "ic_friend"
     
     static let PRIZE_TITLE = "Prize"
     static let PRIZE_DESCRIPTION = "We dont have any prizes yet. All prizes will be displayed here"
     static let PRIZE_IMAGE = "ic_noPrize"
     
     static let CHATS_TITLE = "Chats"
     static let CHATS_DESCRIPTION = "You don't have any active chats yet. All your chats will show up here."
     static let CHATS_IMAGE = "ic_nochat"
     
     static let GROUPS_TITLE = "Groups"
     static let GROUPS_DESCRIPTION = "We don't have any groups yet. All groups will show up here."
     static let GROUPS_IMAGE = "ic_nogroup"
}

struct  AppConstants {
     static let ktimer = 6
     static let ktime = 7
}

struct Notifications {
     static let BUDDYLISTREFRESHED = "buddyListRefreshed"
     static let MESSAGERECEIVED = "messageReceived"
     static let CHATHISTORYRETRIVED = "chatHistory"
}

struct ImagePaths {
     static let USERAVTAR = "myavtar"
}

struct THEME {
     static let BGCOLOR = UIColor.white
     
     static let BUTTON_BGCOLOR = UIColor.white
     
     static let BUTTON_DARK_BGCOLOR = UIColor(displayP3Red:63.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
     
     
     static let BUTTON_HIGHLIGHT_COLOR = TEXT_COLOR
     // static let BUTTON_COLOR = UIColor(colorLiteralRed: 93.0/255.0, green: 110.0/255.0, blue: 129.0/255.0, alpha: 1)
     
     static let BUTTON_COLOR = UIColor(displayP3Red: 76.0/255.0, green: 77.0/255.0, blue: 76.0/255.0, alpha: 1)
     
     static let TEXT_COLOR = UIColor(displayP3Red: 69.0/255.0, green: 67.0/255.0, blue: 68.0/255.0, alpha: 1)
     
     static let BORDER_COLOR = UIColor(displayP3Red: 76.0/255.0, green: 77.0/255.0, blue: 76.0/255.0, alpha: 0.6)
}

enum ErrorTypes: Error {
     case Empty
     case Short
}
}
