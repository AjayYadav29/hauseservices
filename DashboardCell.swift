//
//  DashboardCell.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit

class DashboardCell: UICollectionViewCell {
    @IBOutlet weak var imgCatagories: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
}
