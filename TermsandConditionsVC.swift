//
//  TermsandConditionsVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 30/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit

class TermsandConditionsVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()
//        let url = NSURL (string: "http://propertyturnover.com/terms.pdf")
//        let requestObj = URLRequest(url: url! as URL)
//        webView.loadRequest(requestObj as URLRequest)
        if let pdf = Bundle.main.url(forResource: "Terms of Service October 2017", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            webView.loadRequest(req as URLRequest)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - ActionButton
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
