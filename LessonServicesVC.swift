//
//  LessonServicesVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import ObjectMapper
import NVActivityIndicatorView

class SubcetegoryCell: UITableViewCell {
    
    @IBOutlet weak var subCetegoryLbl: UILabel!
  
}

class LessonServicesVC: UIViewController ,NVActivityIndicatorViewable{
    //MARK: - Outlet
    @IBOutlet weak var lblTitletext: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Private
    var categoryID = 0
    var subCategoryID  = 0
    var titleheder = ""
    var Result : [SubCategory] = []
    var resultSearch : [SubCategory] = []

    var ResultService : [result7] = []
    var refresher:UIRefreshControl!
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    var sectionStats = [Bool](repeating: false, count: 0)
    
    var categories : [Categories] = []

    
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        resultSearch = Result
        sectionStats = [Bool](repeating: false, count: categories.count)
        setUI()
        self.lblTitletext.text = titleheder
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - CustomMethod
    func setUI()
    {
        self.searchBar.delegate = self
    }
    
    func returnMatchService(isClickFromHeader : Bool,indexPath : Int,section : Int) -> (String,String,String,String)  {
        
            if isClickFromHeader {
                for service in ResultService {
                if service.ServiceName == self.Result[indexPath].sub_category {
                    return (service.ServiceID, service.ServiceName, service.RatePerHours,service.ServiceCode)
                }
                }
            }else {
                return ((categories[section].subcategories.innerCategory[indexPath].id), (categories[section].subcategories.innerCategory[indexPath].ServiceName), (categories[section].subcategories.innerCategory[indexPath].RatePerHours),(categories[section].subcategories.innerCategory[indexPath].ServiceName))
                
        }
        return ("","","","")
    }
    
    //MARK: - ActionButton
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}
//MARK: - SearchBarMethod
extension LessonServicesVC : UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        Result = resultSearch
        self.tableView.reloadData()
        self.searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        for view in (searchBar.subviews[0]).subviews{
            if let button = view as? UIButton{
                button.setTitleColor(UIColor.black, for: .normal)
                button.setTitle("Cancel", for:.normal)
            }
        }
        self.searchBar.showsCancelButton = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Result = resultSearch
        if searchText == ""{
            
            self.tableView.reloadData()
        }else{
          
            
            let filteredArrayStruct =  self.Result.filter( { (user: SubCategory) -> Bool in
                return user.sub_category.lowercased().range(of: searchText.lowercased()) != nil
            })
            
            Result = filteredArrayStruct
            self.tableView.reloadData()
        }
    }
    
}
//MARK: - ColletionViewMethod
extension LessonServicesVC : UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard sectionStats[section] else {
            return 0
        }
        return categories[section].subcategories.innerCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubcetegoryCell") as! SubcetegoryCell
        cell.subCetegoryLbl.text =  categories[indexPath.section].subcategories.innerCategory[indexPath.row].sub_category
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GolfLessonsVC") as! GolfLessonsVC
        obj.mainCat = titleheder
        obj.service = returnMatchService(isClickFromHeader: false, indexPath: indexPath.row, section: indexPath.section).1
        obj.serviceID = returnMatchService(isClickFromHeader: false, indexPath: indexPath.row, section: indexPath.section).0
        obj.titleheader = returnMatchService(isClickFromHeader: false, indexPath: indexPath.row, section: indexPath.section).1
        obj.pricecost = returnMatchService(isClickFromHeader: false, indexPath: indexPath.row, section: indexPath.section).2
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView(section: section, title:  categories[section].ServiceName)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       return 15
    }
    
    private func headerView(section: Int, title: String) -> UIView {
        let button = UIButton(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 10, height: 44))
        button.tag = section
        button.setTitle("     \(title)", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(sectionHeaderTapped), for: .touchUpInside)
        button.backgroundColor = UIColor.white
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.cgColor
        return button
    }
    
    @objc private func sectionHeaderTapped(sender: UIButton) {
       
        getData(category: categories[sender.tag], section: sender.tag)
    }
    
    //MARK: - Api
    func getData(category:Categories,section:Int)
    {
        self.sectionStats[section] = !self.sectionStats[section]

        
        if self.sectionStats[section] {
            self.startAnimating()
            AFWrapper.requestPOSTURL(Constants.URLS.Service, params: ["CategoryId": category.main_category as AnyObject,"SubcategoryID": category.ServiceName as AnyObject], headers: nil, success: { (ResponseJson) in
                self.stopAnimating()

                category.subcategories = Mapper<Subcategories>().map(JSONObject: ResponseJson.rawValue)!
                if category.subcategories.innerCategory[0].sub_category == "" {
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "GolfLessonsVC") as! GolfLessonsVC
                    obj.mainCat = self.titleheder
                    obj.service = category.ServiceName
                    obj.serviceID = category.id
                    obj.titleheader = category.ServiceName
                    obj.pricecost = category.RatePerHours
                    obj.serviceCode = ""
                   self.navigationController?.pushViewController(obj, animated: true)
                    return
                }
                if category.subcategories.Status == 1{
                    
                    self.tableView.beginUpdates()
                    self.tableView.reloadSections([section], with: .automatic)
                    self.tableView.endUpdates()
                    if self.categories[section].subcategories.innerCategory.count == 0 {
                      let obj = self.storyboard?.instantiateViewController(withIdentifier: "GolfLessonsVC") as! GolfLessonsVC
                      obj.mainCat = self.titleheder
                        obj.service = self.returnMatchService(isClickFromHeader: true, indexPath: section, section: 0).1
                        obj.serviceID = self.returnMatchService(isClickFromHeader: true, indexPath: section, section: 0).0
                        obj.titleheader = self.returnMatchService(isClickFromHeader: true, indexPath: section, section: 0).1
                        obj.pricecost = self.returnMatchService(isClickFromHeader: true, indexPath: section, section: 0).2
                        obj.serviceCode = self.returnMatchService(isClickFromHeader: true, indexPath: section, section: 0).3
                      self.navigationController?.pushViewController(obj, animated: true)
                  }
                    
                    
                }else{
                  
                }
            }) { (Error) in
              
            }

        }else {
            self.tableView.beginUpdates()
            self.tableView.reloadSections([section], with: .automatic)
            self.tableView.endUpdates()
        }
        
    }
}
