//
//  ConfirmationVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import NVActivityIndicatorView
class ConfirmationVC: UIViewController,NVActivityIndicatorViewable {

    //MARK: - Outlet
    @IBOutlet weak var lblSendText: TTTAttributedLabel!
    var serviceID = ""
    var serviceCode = 001
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    //MARK: - Private
    var lblState = ""
    var lblLastName = ""
    var lblCity = ""
    var txtZipCode = ""
    var txtEmail = ""
    var txtFullName = ""
    var message = ""
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - ActionButton
    @IBAction func btnOk(_ sender: UIButton) {
        appDelegate.moveToDashBoard()
    }
    @IBAction func btnSendInquiry(_ sender: Any) {
        if isInternetAvailable(){
            sendInquiryApi()
        }
        else{
            
        }
    }
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK: - API
    func sendInquiryApi()
    {
        self.startAnimating()
        AFWrapper.requestPOSTURL(Constants.URLS.AddInquiry, params: ["EmailID": self.txtEmail as AnyObject,
                                                               "firstName": self.txtFullName as AnyObject,
                                                               "LastName":self.lblLastName as AnyObject,
                                                               "StateName":lblState as AnyObject,
                                                               "City": self.lblCity as AnyObject,
                                                               "Zipcode":self.txtZipCode as AnyObject,
                                                               "ServiceID":serviceID as AnyObject,
                                                               "ServiceCode":serviceCode as AnyObject,
                                                               "Message": message as AnyObject], headers: nil, success: { (ResponseJson) in
            
            self.stopAnimating()
                                                              if  ResponseJson["status"].intValue == 1{
                                                                    let obj  = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                                                                    obj.serviceID = self.serviceID
                                                                    self.navigationController?.pushViewController(obj, animated: true)
                                                                }
                                                                else if ResponseJson["status"].intValue == 2{
                                                                self.appDelegate.window?.rootViewController?.view.makeToast(ResponseJson["Message"].stringValue, duration: 2, position: .bottom)
                                                                self.appDelegate.moveToDashBoard()
                                                                
                                                    
                                                                }
            
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
}
