//
//  BTextField.swift
//  BlueCoupon
//
//  Created by Impero IT on 03/06/16.
//  Copyright © 2016 Impero IT. All rights reserved.
//

import UIKit

extension UITextView {
    
     open override func awakeFromNib() {
          if Constants.DeviceType.Is_IPhone_5 {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! - 1 )
          }
          if Constants.DeviceType.Is_IPhone_6 {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 3)
               
          }
          if Constants.DeviceType.Is_IPhone_6P {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 4)
          }
          if Constants.DeviceType.Is_IPad {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 7)
          }
     }
}
func isValidEmail(Emailid:String) -> Bool {
     let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
     
     let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
     return emailTest.evaluate(with: Emailid)
}
func isValidPassword(Password:String) -> Bool {
     let regularExpression = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$"
     let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
     
     return passwordValidation.evaluate(with: Password)
}
extension UIView {
     func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
               self.alpha = 1.0
          }, completion: completion)  }
     
     func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
               self.alpha = 0.0
          }, completion: completion)
     }
}
extension UILabel
{
     var optimalHeight : CGFloat
          {
          get
          {
               let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: CGFloat.greatestFiniteMagnitude))
               
               label.numberOfLines = 0
               label.lineBreakMode = self.lineBreakMode
               label.font = self.font
               label.text = self.text
               
               label.sizeToFit()
               
               return label.frame.height
          }
     }
}

extension UITextField {
     
     
     
     open override func awakeFromNib() {
          if Constants.DeviceType.Is_IPhone_5 {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! - 1 )
          }
          if Constants.DeviceType.Is_IPhone_6 {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 3)
               
          }
          if Constants.DeviceType.Is_IPhone_6P {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 4)
          }
          if Constants.DeviceType.Is_IPad {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 7)
          }
     }
    
   
}
extension UITextField{
     @IBInspectable var placeHolderColor: UIColor? {
          get {
               return self.placeHolderColor
          }
          set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
          }
     }
}

extension String {
    func getDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        return dateFormatter.date(from: self)!
    }
    func heightWithWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedString.Key.font: font], context: nil)
        return actualSize.height
    }
    func WidfthWithHeight(height: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedString.Key.font: font], context: nil)
        return actualSize.width
    }
}

extension NSAttributedString {
    func heightWithWidth(width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], context: nil)
        return actualSize.height
    }
}

extension UILabel {
    func heightWithWidth(width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.heightWithWidth(width: width, font: font)
    }
    
    func heightWithAttributedWidth(width: CGFloat) -> CGFloat {
        guard let attributedText = attributedText else {
            return 0
        }
        return attributedText.heightWithWidth(width: width)
    }
}

extension UILabel {
    open override func awakeFromNib() {
        if Constants.DeviceType.Is_IPhone_5 {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!-1)
        }
          if Constants.DeviceType.Is_IPhone_6 {
               self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 3)

          }
        if Constants.DeviceType.Is_IPhone_6P {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 4)
        }
        if Constants.DeviceType.Is_IPad {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! + 7)
        }
    }
}

extension UIButton {
    func addBorder(){
        self.layer.borderColor = Constants.THEME.BUTTON_COLOR.cgColor
        self.layer.borderWidth = 1
    }
    
//    @objc func addDashedBorder() {
//        let color = Constants.THEME.TEXT_COLOR.cgColor
//
//        let shapeLayer:CAShapeLayer = CAShapeLayer()
//        let frameSize = self.frame.size
//        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
//
//        shapeLayer.bounds = shapeRect
//        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        shapeLayer.strokeColor = color
//        shapeLayer.lineWidth = 2
//        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
//        shapeLayer.lineDashPattern = [6,4]
//        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 2).cgPath
//
//        self.layer.addSublayer(shapeLayer)
//
//    }
    open override func awakeFromNib() {
        
        
        
        if Constants.DeviceType.Is_IPhone_5 {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!-1)
        }
        if Constants.DeviceType.Is_IPhone_6 {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!+3)
        }
        else if Constants.DeviceType.Is_IPhone_6P {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!+4)
        }
        else if Constants.DeviceType.Is_IPad {
          self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!+7)
     }
    }
}

extension Int {
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
extension Float {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
}
extension String {
    var length: Int {
        return (self as NSString).length
    }
    
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        // view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
    }
    func imageWithGradient() -> UIImage{
        let img = self
        
        UIGraphicsBeginImageContext(img.size)
        let context = UIGraphicsGetCurrentContext()
        
        img.draw(at: CGPoint(x: 0, y: 0))
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations:[CGFloat] = [0.50, 1.0]
        //1 = opaque
        //0 = transparent
        let bottom = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).cgColor
        let top = UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        
        let colors = [top,bottom
            ] as CFArray
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: colors, locations: locations)
        
        
        let startPoint = CGPoint(x: img.size.width/2,y: 0)
        let endPoint = CGPoint(x: img.size.width/2,y: img.size.height)
        
        context!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions.init(rawValue: 0))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return image!
        
    }
}

extension UIViewController {
    public func showAlert(title: String?, message: String?, preferredStyle: UIAlertController.Style, alertActions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        for alertAction in alertActions {
            alertController.addAction(alertAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}
extension Date {
    func toWebString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    func toDisplayString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        return dateFormatter.string(from: self)
    }
     func toCalenderHeaderString() -> String {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "MMM-dd"
          return dateFormatter.string(from: self)
     }
    
}
extension Dictionary
{
    public init(keys: [Key], values: [Value])
    {
        precondition(keys.count == values.count)
        
        self.init()
        
        for (index, key) in keys.enumerated()
        {
            self[key] = values[index]
        }
    }
}

extension UIView {
    func addDashedBorder() {
        let color = UIColor.red.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
        
    }
}

