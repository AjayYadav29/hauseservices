//
//  JoinUsProVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 30/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit

class JoinUsProVC: UIViewController {

    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - ActionButton
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
   

}
