//
//  LoginVC.swift
//  Fingerprint
//
//  Created by admin on 03/06/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import ImageSlideshow
import Toast_Swift
import NVActivityIndicatorView
import ObjectMapper
import MessageUI
import Kingfisher
import ActionSheetPicker_3_0

class LoginVC: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var lblWelcomeMsg : UILabel!
    @IBOutlet weak var txtFldName : UITextField!
    @IBOutlet weak var txtFldPassword : UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin : UIButton!
    var refresher:UIRefreshControl!
    var userDefault = UserDefaults.standard

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var shareManager : Globals = Globals.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setUpUI()
        lblWelcomeMsg.text = "Hauseservices"
    }
    
    func setUpUI()  {
        btnLogin.backgroundColor = UIColor.white
        btnLogin.setTitleColor(UIColor.black, for: .normal)
        self.navigationController?.isNavigationBarHidden = true
        txtFldName.cornerRadius = txtFldName.frame.size.height/2
        txtFldPassword.cornerRadius = txtFldName.frame.size.height/2
        btnSignUp.cornerRadius = txtFldName.frame.size.height/2
        btnLogin.cornerRadius = txtFldName.frame.size.height/2
        //        txtFldName.setIconOnTextFieldLeft(UIImage(named: "user")!)
        //        txtFldPassword.setIconOnTextFieldLeft(UIImage(named: "password")!)
        //        Utils.changePlaceholderColor(txtFld: txtFldPassword, text: "Password")
        //        Utils.changePlaceholderColor(txtFld: txtFldName, text: "Employee ID")
        
    }
    
    @IBAction func btnActionSignUp(_ sender: UIButton) {
        //  btnSignUp.backgroundColor = UIColor.white
        //  btnSignUp.setTitleColor(UIColor.black, for: .normal)
        
        //  btnLogin.backgroundColor = UIColor.clear
        //  btnLogin.setTitleColor(UIColor.white, for: .normal)
        let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(loginStoryBoard, animated: true)
    }
    
    @IBAction func btnActionLogin(_ sender: UIButton) {
        getDataApi()
    }
    
    func validateEmail(str:String,strTitle:String) -> Bool {
        if str == "" {
            let alertController = UIAlertController(title: "", message: "Please enter email!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            return false
        } else {
            if Utils.isValidEmail(Emailid: str) {
                return true
            } else {
                let alertController = UIAlertController(title: "", message: "Email is invalid!", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return false
            }
        }
    }
    
    func validatePassword(str:String) -> Bool {
        if str == "" {
            let alertController = UIAlertController(title: "", message: "Please enter password!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return false
        }
        //        else if str.length < 7{
        //            PopupConfirmCommon.showRequestPopup(strMgs: "Password must be greater than 6 characters".localized, strTitle: "signin".localized)
        //            return false
        //        }
        return true
    }
    
    
    @objc func startRefresher()
    {
        if isInternetAvailable()
        {
            refresher.beginRefreshing()
            getDataApi()
        }
        else
        {
            self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
        }
        
    }
    
    func getDataApi()
    {
        let username: String = txtFldName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password: String = txtFldPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if !self.validateEmail(str: txtFldName.text!, strTitle: "signin") {return}
        if !self.validatePassword(str: txtFldPassword.text!) {return}
        
        self.startAnimating()
        let param = ["username": username, "password": password]
        
        AFWrapper.requestPOSTURL(Constants.URLS.loginApi, params: param as [String : AnyObject], headers: nil, success: { (ResponseJson) in
            self.stopAnimating()
            print("ResponseJsonResponseJsonResponseJsonResponseJson\(ResponseJson["map"])")
            
            if ResponseJson["status"] == false {
                let alertController = UIAlertController(title: "", message:"\(ResponseJson["map"])", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }else {
                let login : DashBoardVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                self.navigationController?.pushViewController(login, animated: true)

                self.userDefault.set(nil, forKey: Constants.KEYS.fullname)
                self.userDefault.set(nil, forKey: Constants.KEYS.lastname)
                self.userDefault.set(nil, forKey: Constants.KEYS.email)
                self.userDefault.set(nil, forKey: Constants.KEYS.phoneNumber)
                
                
                self.userDefault.set("\(ResponseJson["map"][0]["first_name"])" , forKey: Constants.KEYS.fullname)
                self.userDefault.set("\(ResponseJson["map"][0]["last_name"])", forKey: Constants.KEYS.lastname)
                self.userDefault.set("\(ResponseJson["map"][0]["email"])", forKey: Constants.KEYS.email)
                self.userDefault.set("\(ResponseJson["map"][0]["phone"])", forKey: Constants.KEYS.phoneNumber)
                
                //            self.userDefault.set("", forKey: Constants.KEYS.state)
                //            self.userDefault.set(self.txtCity.text, forKey: Constants.KEYS.city)
                //            self.userDefault.set(self.txtZipCode.text, forKey: Constants.KEYS.zipcode)
                //            self.userDefault.set(self.textView.text, forKey: Constants.KEYS.message)
                UserDefaults.standard.set(true, forKey: "isAlreadyLogin")
            }
            
            
                     
            
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
}
