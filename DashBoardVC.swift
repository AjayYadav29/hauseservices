//
//  DashBoardVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ImageSlideshow
import Toast_Swift
import NVActivityIndicatorView
import ObjectMapper
import MessageUI
import Kingfisher
import ActionSheetPicker_3_0
class DashBoardVC: UIViewController,NVActivityIndicatorViewable,MFMailComposeViewControllerDelegate {

    //MARK: - Outlet
    let arrstate = ["All 50 states that we serve",
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "Florida",
    "Georgia",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "Washington, DC",
    "West Virginia",
    "Wisconsin",
    "Wyoming"]
    
    
    @IBOutlet weak var sideMenuHowItWork: ImageSlideshow!
    
    @IBOutlet weak var heightCisonalServices: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var imageSaprator: UIImageView!
    @IBOutlet weak var viewSideMenu: UIView!
    @IBOutlet weak var btnSideMenuTapObj: UIButton!
    @IBOutlet weak var sideMenuImg: UIImageView!
    @IBOutlet weak var btnSideMenuObj: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var slideView: ImageSlideshow!
    @IBOutlet weak var colletionView: UICollectionView!
    @IBOutlet weak var ColletionViewSeasonal: UICollectionView!
    var Result : [result] = []
    var inner_categoryArr : [result7] = []
    var inner_categorySearchArr : [result7] = []

    var arrimage : [ImageSource] = []
    var arrHowItwork : [ImageSource] = []
    var ResultService : [result7] = []
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    var refresher:UIRefreshControl!

    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideView.addGestureRecognizer(gestureRecognizer)
        userDefault.set(true, forKey: Constants.KEYS.INTROVIEW)
        setSlideImage()
        setUI()
        hideSideMenu()
        setrefresh()
        if isInternetAvailable(){
            getDataApi()
            getServiceList()
        }
        else{
            self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(getReloadData), name: NSNotification.Name(rawValue: "reload"), object: nil)

    }

    @objc func getReloadData(){
        //self.sideMenuHowItWork.setCurrentPage(1, animated: true)
        setSlideImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
//        if Constants.DeviceType.Is_IPad{
//            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1800)
//        }
      //  appDelegate.GetCurrentLocation()
    }
    
    //MARK: - CustomMethod
    func setUI()
    {
        self.tableView.isHidden = true
        self.searchBar.showsCancelButton = false
       // self.searchBar.isHidden = true
        self.searchBar.delegate = self
    }
    func hideSideMenu()
    {
        self.btnSideMenuTapObj.isHidden = true
        self.sideMenuImg.isHidden = true
        self.viewSideMenu.isHidden = true
        self.btnSideMenuObj.isHidden = true
        self.imageSaprator.isHidden = true
        
    }
    func setrefresh()
    {
        refresher = UIRefreshControl()
        self.colletionView!.alwaysBounceVertical = true
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(startRefresher), for: .valueChanged)
        colletionView!.addSubview(refresher)
        //refresher.beginRefreshing()
    }
    @objc func startRefresher()
    {
        if isInternetAvailable()
        {
            refresher.beginRefreshing()
            getDataApi()
        }
        else
        {
            self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
        }
        
    }
    
    func setSlideImage()
    {
        arrHowItwork.removeAll()
        for i in 0 ..< 4 {
            if i == 0{
                arrHowItwork.append(ImageSource(image: UIImage(named: "how1.PNG")!))
            }
            else if i == 1{
                arrHowItwork.append(ImageSource(image: UIImage(named: "how2.PNG")!))
            }
            else if i == 2{
                arrHowItwork.append(ImageSource(image: UIImage(named: "how3.PNG")!))
            }
            else if i == 3{
                arrHowItwork.append(ImageSource(image: UIImage(named: "how4.PNG")!))
            }
        }
        self.sideMenuHowItWork.setCurrentPage(0, animated: true)
        self.sideMenuHowItWork.backgroundColor = UIColor.white
       // self.sideMenuHowItWork.slideshowInterval = 3.0
        self.sideMenuHowItWork.contentScaleMode = UIView.ContentMode.scaleToFill
        self.sideMenuHowItWork.setImageInputs(arrHowItwork)
        
        self.sideMenuHowItWork.draggingEnabled  = true
    }
    
    //MARK: - ActionButton
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        print(result)
        controller.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnMailSent(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["customerservice@propertyyurnover.com"])
           // mail.setSubject("Email")
         //   mail.setSubject("ledapp – rethink lighting™")
           // mail.setMessageBody("<p>Check out how much I can save with replacing my old bulbs with led using ledapp – rethink lighting™</p>", isHTML: true)
            present(mail, animated: true, completion: nil)
        }
        else
        {
            let alertController = UIAlertController(title: "PropertyTurnover", message: "Configure your mail.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    @IBAction func btnSelectState(_ sender: Any) {
        
        var index = 0
        for i in 0 ..< arrstate.count {
            if self.lblState.text == arrstate[i]{
                index = i
                break
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "Select City", rows: self.arrstate, initialSelection: index, doneBlock: {
            picker, value, index in
            
            self.lblState.text = index! as! String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
   // @IBAction func btnSearch(_ sender: Any) {
      //  self.searchBar.isHidden = false
    //    let desiredOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
      //  self.scrollView.setContentOffset(desiredOffset, animated: true)
    //}
    @IBAction func btnSideMenu(_ sender: Any) {
        self.btnSideMenuTapObj.isHidden = false
        self.sideMenuImg.isHidden = false
        self.viewSideMenu.isHidden = false
        self.btnSideMenuObj.isHidden = false
        self.imageSaprator.isHidden = false
        self.tableView.isHidden = true
    }
    @IBAction func btnSideMenuTap(_ sender: Any) {
        hideSideMenu()
    }
    @IBAction func btnSideMenutapHide(_ sender: Any) {
        hideSideMenu()
    }
    @IBAction func btnAboutUS(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnTermsandCondtions(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TermsandConditionsVC") as! TermsandConditionsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnPrivacy(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnUsetheapp(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewVC") as! IntroViewVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnJoinUsasPro(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "https://www.hauseservices.com/pro")!)
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "JoinUsProVC") as! JoinUsProVC
//        self.navigationController?.pushViewController(obj, animated: true)
    }
    
      @IBAction func btnActionLogOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isAlreadyLogin")
        let login : LoginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)

        }
    
    //MARK: - Api
    func getServiceList()
    {
        self.startAnimating()
        AFWrapper.requestGETURL(Constants.URLS.AllServices, success: { (ResponseJson) in
            self.stopAnimating()
            self.shareManager.getServiceList = Mapper<getServiceListModel>().map(JSONObject: ResponseJson.rawValue)!
            if self.shareManager.getServiceList.Status == 1{
                self.ResultService = self.shareManager.getServiceList.Result
                self.inner_categorySearchArr = self.ResultService
            }else{
                self.view.makeToast(self.shareManager.getDashBoardData.Message, duration: 2, position: .bottom)
            }
        }) { (Error) in
            self.refresher.endRefreshing()
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
    func getDataApi()
    {
        self.startAnimating()
       AFWrapper.requestGETURL(Constants.URLS.viewCategory, success: { (ResponseJson) in
            self.stopAnimating()
            self.refresher.endRefreshing()

            self.shareManager.getDashBoardData = Mapper<getDashBoardDataModel>().map(JSONObject: ResponseJson.rawValue)!
        for i in 0..<self.shareManager.getDashBoardData.Result.count {
            let subCat = self.shareManager.getDashBoardData.Result[i].sub_category
            for i in 0..<subCat.count {
                let subCategory = subCat[i]
                let subCatAdd = Innercategory()
                subCatAdd.cat_id = subCategory.cat_id
                subCatAdd.inner_category = subCategory.sub_category
                subCatAdd.cat_id = subCategory.cat_id
              //  self.inner_categoryArr.append(subCatAdd)
                let innerCat = subCat[i].inner_category
                for i in innerCat {
                    let newCat = result7()
                    newCat.ServiceID = i.cat_id
                    newCat.RatePerHours = i.RatePerHours
                    newCat.ServiceName = i.inner_category
//                    newCat.ServiceID = i.cat_id
//                    newCat.ServiceID = i.cat_id
                    self.inner_categoryArr.append(newCat)
                }
            }
        }
        self.inner_categorySearchArr = self.inner_categoryArr

        if self.shareManager.getDashBoardData.Status == 1{
           self.Result = self.shareManager.getDashBoardData.Result
            self.colletionView.reloadData()
            self.getSliderImg()
        }else{
            self.colletionView.reloadData()
            self.view.makeToast(self.shareManager.getDashBoardData.Message, duration: 2, position: .bottom)
        }
       }) { (Error) in
            self.refresher.endRefreshing()
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
      
    }
    func getSeasonalImg()
    {
        self.startAnimating()
        AFWrapper.requestGETURL(Constants.URLS.Seasonal, success: { (ResponseJson) in
            self.stopAnimating()
            self.shareManager.getSeasonal = Mapper<getSeasonalModel>().map(JSONObject: ResponseJson.rawValue)!
            
            if self.shareManager.getSeasonal.Result.count > 0{
                self.ColletionViewSeasonal.reloadData()
            }else{
                self.view.makeToast(self.shareManager.getSeasonal.Message
                    , duration: 2, position: .bottom)
            }
            
        
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
    func getSliderImg()
    {
        self.startAnimating()
            AFWrapper.requestGETURL(Constants.URLS.TopServices, success: { (ResponseJson) in
                self.stopAnimating()
                self.shareManager.getBestServices = Mapper<getTopServices>().map(JSONObject: ResponseJson.rawValue)!
                self.arrimage.removeAll()
                for i in 0 ..< self.shareManager.getBestServices.Result.count {
                    let img = UIImageView()
                    let imgsource = (self.shareManager.getBestServices.Result[i] as! TopServices).img_url
                    let url = URL(string: imgsource)
                   
                    KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        if image != nil{
                            self.getimg(img: image!)
                        }
                    })
                    
                }
                self.getSeasonalImg()
            }) { (Error) in
                self.stopAnimating()
                self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
    func getimg(img: UIImage){
        
        
        arrimage.append(ImageSource(image: img))
        self.slideView.backgroundColor = UIColor.white
        self.slideView.slideshowInterval = 3.0
        self.slideView.contentScaleMode = UIView.ContentMode.scaleToFill
        self.slideView.setImageInputs(arrimage)
        
        self.slideView.draggingEnabled  = true
    }
    @objc func didTap(){
        let tag = slideView.currentPage
        print(tag)
        var selectedCat = String()
        switch tag{
        case 0:
            selectedCat = "Event Planning"
        case 1:
            selectedCat = "Furniture"
        case 2:
            selectedCat = "Accessibility"
        case 3:
            selectedCat = "Flooring"
            
        default:
            selectedCat = ""
        }
        print(String(self.shareManager.getBestServices.Result[tag].category_name))
            self.startAnimating()
        AFWrapper.requestPOSTURL(Constants.URLS.Subcategory, params: ["CategoryID": selectedCat as AnyObject], headers: nil, success: { (ResponseJson) in

                
                self.stopAnimating()
                self.shareManager.getLession = Mapper<LessionModel>().map(JSONObject: ResponseJson.rawValue)!
                if self.shareManager.getLession.Status == 1{
                    if self.shareManager.getLession.Result.count == 0{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
                        obj.titleheder = self.Result[tag].CategoryName
                        obj.categoryId = self.Result[tag].CategoryId
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
                        
                        obj.titleheder = selectedCat
                      //  obj.Result = self.Result[self.slideView.currentPage].sub_category
                        obj.ResultService =  self.ResultService
                        obj.categories = self.shareManager.getLession.Result
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else{
                    self.view.makeToast(self.shareManager.getLession.Message, duration: 0.2, position: .bottom)
    //                if self.shareManager.getLession.Result.count == 0{
    //                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
    //                    obj.titleheder = self.Result[tag].CategoryName
    //                    obj.categoryId = self.Result[tag].CategoryId
    //                    self.navigationController?.pushViewController(obj, animated: true)
    //                }else{
    //                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
    //                    obj.titleheder = self.Result[tag].CategoryName
    //                    obj.Result = self.shareManager.getLession.Result
    //                    self.navigationController?.pushViewController(obj, animated: true)
    //                }
                }
                
            }) { (Error) in
                self.stopAnimating()
                self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
            }
        }
    func gotoLession(tag : Int)
    {
        self.startAnimating()
        AFWrapper.requestPOSTURL(Constants.URLS.Subcategory, params: ["CategoryID": String(self.Result[tag].CategoryName) as AnyObject], headers: nil, success: { (ResponseJson) in

            
            self.stopAnimating()
            self.shareManager.getLession = Mapper<LessionModel>().map(JSONObject: ResponseJson.rawValue)!
            if self.shareManager.getLession.Status == 1{
                if self.shareManager.getLession.Result.count == 0{
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
                    obj.titleheder = self.Result[tag].CategoryName
                    obj.categoryId = self.Result[tag].CategoryId
                    self.navigationController?.pushViewController(obj, animated: true)
                }else{
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
                    
                    obj.titleheder = self.Result[tag].CategoryName
                    obj.Result = self.Result[tag].sub_category
                    obj.ResultService =  self.ResultService
                    obj.categories = self.shareManager.getLession.Result
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            else{
                self.view.makeToast(self.shareManager.getLession.Message, duration: 0.2, position: .bottom)
//                if self.shareManager.getLession.Result.count == 0{
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
//                    obj.titleheder = self.Result[tag].CategoryName
//                    obj.categoryId = self.Result[tag].CategoryId
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }else{
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
//                    obj.titleheder = self.Result[tag].CategoryName
//                    obj.Result = self.shareManager.getLession.Result
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }
            }
            
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
    
    
        func gotHomeImprovement(tag : Int)
        {
            self.startAnimating()
            AFWrapper.requestPOSTURL(Constants.URLS.Subcategory, params: ["CategoryID": String(self.shareManager.getSeasonal.Result[tag].category_name) as AnyObject], headers: nil, success: { (ResponseJson) in

                
                self.stopAnimating()
                self.shareManager.getLession = Mapper<LessionModel>().map(JSONObject: ResponseJson.rawValue)!
                if self.shareManager.getLession.Status == 1{
                    if self.shareManager.getLession.Result.count == 0{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
                        obj.titleheder = self.Result[tag].CategoryName
                        obj.categoryId = self.Result[tag].CategoryId
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
                        
                        obj.titleheder = self.shareManager.getSeasonal.Result[tag].category_name
                        //obj.Result = self.Result[tag].sub_category
                        obj.ResultService =  self.ResultService
                        obj.categories = self.shareManager.getLession.Result
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else{
                    self.view.makeToast(self.shareManager.getLession.Message, duration: 0.2, position: .bottom)
    //                if self.shareManager.getLession.Result.count == 0{
    //                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "FitnessVC") as! FitnessVC
    //                    obj.titleheder = self.Result[tag].CategoryName
    //                    obj.categoryId = self.Result[tag].CategoryId
    //                    self.navigationController?.pushViewController(obj, animated: true)
    //                }else{
    //                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LessonServicesVC") as! LessonServicesVC
    //                    obj.titleheder = self.Result[tag].CategoryName
    //                    obj.Result = self.shareManager.getLession.Result
    //                    self.navigationController?.pushViewController(obj, animated: true)
    //                }
                }
                
            }) { (Error) in
                self.stopAnimating()
                self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
            }
        }
    
}
//MARK: - SearchBarMethod
extension DashBoardVC : UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
       // self.searchBar.isHidden = true
        self.tableView.isHidden = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        for view in (searchBar.subviews[0]).subviews{
            if let button = view as? UIButton{
                button.setTitleColor(UIColor.black, for: .normal)
                button.setTitle("Cancel", for:.normal)
            }
        }
        self.searchBar.showsCancelButton = true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            inner_categoryArr = inner_categorySearchArr
            self.tableView.isHidden = true
            self.ResultService = self.shareManager.getServiceList.Result
            self.tableView.reloadData()
        }else{
            //self.tableView.isHidden = false
            inner_categoryArr = inner_categorySearchArr
            
            let filteredArrayStruct =  inner_categoryArr.filter( { (user: result7) -> Bool in
                return user.ServiceName.lowercased().range(of: searchText.lowercased()) != nil
            })
            
            inner_categoryArr = filteredArrayStruct
            if inner_categoryArr.count > 0{
                self.tableView.isHidden = false
            }else{
                self.tableView.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
}
//MARK: - tableViewMethod
extension DashBoardVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inner_categoryArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.inner_categoryArr[indexPath.row].ServiceName
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GolfLessonsVC") as! GolfLessonsVC
        if Int(self.inner_categoryArr[indexPath.row].ServiceID) != nil {
          obj.serviceID = self.inner_categoryArr[indexPath.row].ServiceID
        }
       // obj.service = self.inner_categoryArr[indexPath.row].inner_category
        
        obj.titleheader = self.inner_categoryArr[indexPath.row].ServiceName
        obj.pricecost = self.inner_categoryArr[indexPath.row].RatePerHours
        obj.mainCat = self.inner_categoryArr[indexPath.row].ServiceName
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - CollletionViewMethod
extension DashBoardVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ColletionViewSeasonal{
            colletionView.SetTableViewBlankLable(count:self.Result.count , str: "No Data Found.")
            return self.shareManager.getSeasonal.Result.count
        }else{
            colletionView.SetTableViewBlankLable(count:self.Result.count , str: "No Data Found.")
            return self.Result.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DashboardCell
        if collectionView == ColletionViewSeasonal{
            let url = URL(string: self.shareManager.getSeasonal.Result[indexPath.row].img_url)
            cell.imgCatagories.kf.indicatorType = .activity
            cell.imgCatagories.kf.setImage(with: url)
        }else{
            let url = URL(string: self.Result[indexPath.row].ImageName)
            cell.imgCatagories.kf.indicatorType = .activity
            cell.imgCatagories.kf.setImage(with: url)
        }
        
         //   cell.imgCatagories.image = #imageLiteral(resourceName: "IMG_2019 (1).JPG")
          //  cell.lblName.text = "Plumbing"
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ColletionViewSeasonal{
            gotHomeImprovement(tag: indexPath.row)
        }else{
            if isInternetAvailable(){
                gotoLession(tag: indexPath.row)
            }else{
                self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ColletionViewSeasonal{
            if Constants.DeviceType.Is_IPhone_4_OR_Less{
                return CGSize(width: self.ColletionViewSeasonal.frame.size.width / 3.0, height: self.ColletionViewSeasonal.frame.size.height / 2.7)
            }
           else if Constants.DeviceType.Is_IPhone_5{
                return CGSize(width: self.ColletionViewSeasonal.frame.size.width / 3.0, height: self.ColletionViewSeasonal.frame.size.height / 2.5)
            }
           else if Constants.DeviceType.Is_IPhone_6{
                return CGSize(width: self.ColletionViewSeasonal.frame.size.width / 3.0, height: self.ColletionViewSeasonal.frame.size.height / 2.6)
            }
           else if Constants.DeviceType.Is_IPhone_6P{
                return CGSize(width: self.ColletionViewSeasonal.frame.size.width / 3.0, height: self.ColletionViewSeasonal.frame.size.height / 2.6)
            }
            else{
                return CGSize(width: self.ColletionViewSeasonal.frame.size.width / 3.0, height: self.ColletionViewSeasonal.frame.size.height / 2.6)
            }
            
        }else{
            if Constants.DeviceType.Is_IPhone_6P{
                return CGSize(width: self.colletionView.frame.size.width / 3.0, height: self.colletionView.frame.size.height / 2.7)
            }
            else if Constants.DeviceType.Is_IPhone_5{
                return CGSize(width: self.colletionView.frame.size.width / 3.0, height: self.colletionView.frame.size.height / 2.5)
            }
            else if Constants.DeviceType.Is_IPhone_6{
                return CGSize(width: self.colletionView.frame.size.width / 3.0, height: self.colletionView.frame.size.height / 2.6)
            }
            else{
                return CGSize(width: self.colletionView.frame.size.width / 3.0, height: self.colletionView.frame.size.height / 2.6)
            }
        }
    }
    
}


