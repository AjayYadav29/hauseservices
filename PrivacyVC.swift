//
//  PrivacyVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 05/10/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit

class PrivacyVC: UIViewController ,UIWebViewDelegate{

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        let url = NSURL (string: "http://propertyturnover.com/policy.pdf")
//        let requestObj = URLRequest(url: url! as URL)
//        webView.loadRequest(requestObj as URLRequest)
      //  webView.delegate = self
        if let pdf = Bundle.main.url(forResource: "Privacy Policy October 24 2017", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            //webView.dataDetectorTypes = .link
            webView.loadRequest(req as URLRequest)
        }
        // Do any additional setup after loading the view.
    }
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        let req = request.url
//        if navigationType == UIWebViewNavigationType.linkClicked{
//            UIApplication.shared.openURL(req!)
//            return false
//        }else{
//            return true
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
