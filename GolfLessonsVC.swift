//
//  GolfLessonsVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Toast_Swift
import ObjectMapper
import NVActivityIndicatorView
import CoreLocation
class GolfLessonsVC: UIViewController ,NVActivityIndicatorViewable,UITextViewDelegate,CLLocationManagerDelegate{
    
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var txtLastName: TextField!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lblTitletextHeder: UILabel!
    var mainCat = ""
    var service = ""
    
    
    
    //MARK: - Outlet
    @IBOutlet weak var txtCity: TextField!
    @IBOutlet weak var lblState: TextField!
    @IBOutlet weak var txtZipCode: TextField!
    @IBOutlet weak var txtEmail: TextField!
    @IBOutlet weak var txtFullName: TextField!
    @IBOutlet weak var txtPhoneNumber: TextField!
    
    
    
    var serviceID = ""
    var serviceCode = ""
    var stateId = 0
    var locationManager = CLLocationManager()
    var coordinate : CLLocationCoordinate2D!
    var placeholderLabel : UILabel!
    var pricecost = ""
    var titleheader = ""
    var stateArr : [String] = []
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        placeholderlable()
        self.textView.delegate = self
        self.lblServiceName.text = titleheader
        //  self.lblTitletextHeder.text = titleheader
        setData()
        self.lblprice.text = "The estimated cost is " + pricecost  + ". Please complete the form below and receive up to 5 accurate quotes from Pros" + "." + " for Free"
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.textView.text = ""
        self.placeholderLabel.isHidden = false
    }
    //MARK: - CustomMethod
    func setData()
    {
        if  self.userDefault.string(forKey: Constants.KEYS.email) != nil{
            //self.lblState.text = self.userDefault.string(forKey: Constants.KEYS.state)
            //  self.txtZipCode.text = self.userDefault.string(forKey: Constants.KEYS.zipcode)
            self.txtFullName.text = self.userDefault.string(forKey: Constants.KEYS.fullname)
            self.txtLastName.text = self.userDefault.string(forKey: Constants.KEYS.lastname)
            self.txtEmail.text = self.userDefault.string(forKey: Constants.KEYS.email)
            self.txtPhoneNumber.text = self.userDefault.string(forKey: Constants.KEYS.phoneNumber)
            if (self.shareManager.stateglobal) == "" {
                
            }else {
                self.lblState.text = "\(self.shareManager.stateglobal)"
            }
            self.txtCity.text = self.shareManager.cityglobal
            self.txtZipCode.text = self.shareManager.zipcodeglobal
            //  self.txtCity.text = self.userDefault.string(forKey: Constants.KEYS.city)
            // self.textView.text = self.userDefault.string(forKey: Constants.KEYS.message)
        }else{
            if (self.shareManager.stateglobal) == "" {
          
            }else {
                self.lblState.text = "\(self.shareManager.stateglobal)"
            }
            self.txtCity.text = self.shareManager.cityglobal
            self.txtZipCode.text = self.shareManager.zipcodeglobal
            self.textView.text = ""
            self.placeholderLabel.isHidden = false
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    func placeholderlable()
    {
        placeholderLabel = UILabel()
        
        placeholderLabel.text = "Your Comments"
        if Constants.DeviceType.Is_IPhone_4_OR_Less{
            placeholderLabel.font = UIFont(name: "SFUIDisplay-Regular", size: 14.0)
        }
        if Constants.DeviceType.Is_IPhone_5{
            placeholderLabel.font = UIFont(name: "SFUIDisplay-Regular", size: 15.0)
        }
        if Constants.DeviceType.Is_IPhone_6{
            placeholderLabel.font = UIFont(name: "SFUIDisplay-Regular", size: 17.0)
        }
        if Constants.DeviceType.Is_IPhone_6P {
            placeholderLabel.font = UIFont(name: "SFUIDisplay-Regular", size: 18.0)
        }
        if Constants.DeviceType.Is_IPad {
            placeholderLabel.font = UIFont(name: "SFUIDisplay-Regular", size: 20.0)
        }
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 3, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.gray
        placeholderLabel.isHidden = !textView.text.isEmpty
        
    }
    func getState()
    {   self.startAnimating()
        AFWrapper.requestGETURL(Constants.URLS.getState, success: { (ResponseJson) in
            self.stopAnimating()
            self.shareManager.getState = Mapper<StateModel>().map(JSONObject: ResponseJson.rawValue)!
            if self.shareManager.getState.Status == 1{
                for i in self.shareManager.getState.Result{
                    self.stateArr.append(i.StateName)
                }
            }
            else{
                self.view.makeToast(self.shareManager.getState.Message, duration: 2, position: .bottom)
                
            }
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
    }
    
    //MARK: - ActionButton
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnState(_ sender: Any) {
        //        ActionSheetStringPicker.show(withTitle: "Select State", rows: self.stateArr, initialSelection: 1, doneBlock: {
        //            picker, value, index in
        //
        //            self.lblState.text = index! as! String
        //            return
        //        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func btnCity(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select City", rows: self.stateArr, initialSelection: 1, doneBlock: {
            picker, value, index in
            
            self.txtCity.text = index! as! String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func btnSendEnquiry(_ sender: Any) {
        if isInternetAvailable(){
            sendInquiryApi()
        }
        else{
            self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
        }
    }
    @IBAction func btnTermsofService(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TermsandConditionsVC") as! TermsandConditionsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnPrivacy(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK: - Api
    func sendInquiryApi()
    {
        var isValid = true
        
        if Validation.validation.isEmpty(self.txtFullName.text!){
            isValid = false
            self.view.makeToast("Please enter First name", duration: 2, position: .bottom)
        }
        else if Validation.validation.isEmpty(self.txtLastName.text!){
            isValid = false
            self.view.makeToast("Please enter Last name", duration: 2, position: .bottom)
        }
        else if Validation.validation.isEmpty(self.txtEmail.text!) || !Validation.validation.isValidEmail(self.txtEmail.text!)
        {
            isValid = false
            if Validation.validation.isEmpty(self.txtEmail.text!)
            {
                self.view.makeToast("Please enter Email", duration: 2, position: .bottom)
            }
            else{
                self.view.makeToast("Please enter valid EmailId", duration: 2, position: .bottom)
            }
        }
        else if Validation.validation.isEmpty(lblState.text!){
            isValid = false
            self.view.makeToast("Please select State", duration: 2, position: .bottom)
        }
        else if Validation.validation.isEmpty(txtCity.text!){
            isValid = false
            self.view.makeToast("Please select City", duration: 2, position: .bottom)
        }
        else if Validation.validation.isEmpty(self.txtZipCode.text!){
            isValid = false
            self.view.makeToast("Please enter Zip code", duration: 2, position: .bottom)
        }
        
        if isValid{
            
            var j = 0
            for i in stateArr{
                if self.lblState.text == i{
                    stateId = self.shareManager.getState.Result[j].StateID
                }
                j = j + 1
            }

            self.startAnimating()
            AFWrapper.requestPOSTURL(Constants.URLS.AddInquiry, params: ["EmailID": self.txtEmail.text as AnyObject,
                                                                                     "firstName": self.txtFullName.text as AnyObject,
                                                                                     "LastName":self.txtLastName.text as AnyObject,
                                                                                     "StateName":self.lblState.text as AnyObject,
                                                                                     "City": self.txtCity.text as AnyObject ,
                                                                                     "Zipcode":self.txtZipCode.text as AnyObject,
                                                                                     "mobile":self.txtPhoneNumber.text as AnyObject,
                                                                                     "service_cat":service as AnyObject,
                                                                                     "mainc":mainCat as AnyObject,
                                                                                     "price": pricecost as AnyObject,
                                                                                     "submit" : "1" as AnyObject,
                                                                                     "Message":self.textView.text as AnyObject], headers: nil, success: { (ResponseJson) in
                                                                                        self.stopAnimating()
                                                                                        if ResponseJson["status"] == true {
                                                                                            
                                                                                            self.userDefault.set(self.txtFullName.text, forKey: Constants.KEYS.fullname)
                                                                                            self.userDefault.set(self.txtLastName.text, forKey: Constants.KEYS.lastname)
                                                                                            self.userDefault.set(self.txtEmail.text, forKey: Constants.KEYS.email)
                                                                                            self.userDefault.set(self.lblState.text, forKey: Constants.KEYS.state)
                                                                                            self.userDefault.set(self.txtCity.text, forKey: Constants.KEYS.city)
                                                                                            self.userDefault.set(self.txtZipCode.text, forKey: Constants.KEYS.zipcode)
                                                                                            self.userDefault.set(self.txtPhoneNumber.text, forKey: Constants.KEYS.phoneNumber)
            //self.userDefault.set(self.textView.text, forKey: Constants.KEYS.message)
                                                                                            
                                                                                        }
                                                                                        if ResponseJson["status"] == true{
                                                                                            let obj  = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                                                                                            obj.serviceID = self.serviceID
                                                                                            obj.lblCity = self.txtCity.text!
                                                                                            obj.lblState = self.lblState.text!
                                                                                            obj.txtEmail = self.txtEmail.text!
                                                                                            obj.txtFullName = self.txtFullName.text!
                                                                                            obj.lblLastName = self.txtLastName.text!
                                                                                            obj.txtZipCode = self.txtZipCode.text!
                                                                                            obj.message = self.textView.text
                                                                                            self.navigationController?.pushViewController(obj, animated: true)
                                                                                        }
                                                                                        else if ResponseJson["Status"] == false{
                                                                                            
                                                                                            
                                                                                            self.view.makeToast(ResponseJson["Message"].stringValue, duration: 2, position: .bottom)
                                                                                            self.appDelegate.moveToDashBoard()
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                        }, failure: { (Error) in
                            self.stopAnimating()
                            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
                        })
            
            
        }
        
    }
}

