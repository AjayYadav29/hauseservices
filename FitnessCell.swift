//
//  FitnessCell.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit

class FitnessCell: UITableViewCell {

    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArcCode: UILabel!
    @IBOutlet weak var imgFitness: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
