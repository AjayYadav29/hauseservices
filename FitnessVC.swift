//
//  FitnessVC.swift
//  PropertyTurnover
//
//  Created by Impero IT on 29/08/17.
//  Copyright © 2017 Impero IT. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import ObjectMapper
import Kingfisher
class FitnessVC: UIViewController,NVActivityIndicatorViewable {

    //MARK: - Outlet
    @IBOutlet weak var lbltitletextheder: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var imgFitness: UIImageView!
    //MARK: - Private
    var categoryId = ""
    var subCategory = ""
    var titleheder = ""
    var Result : [result2] = []
    //MARK: - GolbalUse
    var shareManager : Globals = Globals.sharedInstance
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDefault = UserDefaults.standard
    //MARK: - DefaultMethod
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        self.lbltitletextheder.text = titleheder
        if isInternetAvailable(){
            getData()
        }
        else{
            self.view.makeToast(Constants.Errors.ERROR_INTERNTE, duration: 2, position: .bottom)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - CustomMethod
    func setUI()
    {
        self.searchBar.delegate = self
        self.searchBar.isHidden = true
        self.tableView.tableFooterView = UIView()
    }
    //MARK: - ActionButton
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Api
    func getData()
    {
        self.startAnimating()
        AFWrapper.requestPOSTURL(Constants.URLS.Service, params: ["CategoryId": String(categoryId) as AnyObject,"SubcategoryID": "Driver" as AnyObject], headers: nil, success: { (ResponseJson) in
            self.stopAnimating()
            self.shareManager.getFitess = Mapper<FitnessModel>().map(JSONObject: ResponseJson.rawValue)!
            if self.shareManager.getFitess.Status == 1{
                self.Result = self.shareManager.getFitess.Result
                self.tableView.reloadData()
            }else{
                self.tableView.reloadData()
                self.view.makeToast(self.shareManager.getFitess.Message, duration: 2, position: .bottom)
            }
        }) { (Error) in
            self.stopAnimating()
            self.view.makeToast(Constants.Errors.ERROR_TIMEOUT, duration: 2, position: .bottom)
        }
        
    }
}
//MARK: - SearchBarMethod
extension FitnessVC : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        for view in (searchBar.subviews[0]).subviews{
            if let button = view as? UIButton{
                button.setTitleColor(UIColor.black, for: .normal)
                button.setTitle("Cancel", for:.normal)
            }
        }
        self.searchBar.showsCancelButton = true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            Result = []
            Result = self.shareManager.getFitess.Result
            self.tableView.reloadData()
        }else{
            Result = []
            
            let filteredArrayStruct =  self.shareManager.getFitess.Result.filter( { (user: result2) -> Bool in
                return user.ServiceName.lowercased().range(of: searchText.lowercased()) != nil
            })
            
            Result = filteredArrayStruct
            self.tableView.reloadData()
        }
    }
}
//MARK: - tableViewMethod
extension FitnessVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.SetTableViewBlankLable(count:self.Result.count , str: "No Data Found.")
        return self.Result.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FitnessCell
        
        cell.lblTitle.text = self.Result[indexPath.row].ServiceName
        cell.selectionStyle = .none
      //  cell.lblCost.text = self.Result[indexPath.row].ServiceCode
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GolfLessonsVC") as! GolfLessonsVC
        obj.serviceID = self.Result[indexPath.row].ServiceID
        obj.titleheader = self.Result[indexPath.row].ServiceName
        obj.pricecost = self.Result[indexPath.row].RatePerHours
        self.navigationController?.pushViewController(obj, animated: true)
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if Constants.DeviceType.Is_IPad{
//            return 67 + 20
//        }
//        else if Constants.DeviceType.Is_IPhone_6P{
//            return 67 
//        }
//        else{
//            return 67
//        }
//    }
}
